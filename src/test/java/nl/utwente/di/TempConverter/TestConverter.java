package nl.utwente.di.TempConverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestConverter {
    @Test
    public void testtemp() throws Exception {
        Temperature quoter = new Temperature();
        double temp= quoter.getFarenheit("1");
        Assertions.assertEquals((1*1.8)+32, temp,0.0, "Price of book 1");
    }
}
